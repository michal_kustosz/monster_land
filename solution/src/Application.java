import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class Application {
	
	private static final String TEASURES_FILENAME = "treasures.txt";
	private static final String MONSTERS_FILENAME = "monsters.txt";
	
	private ArrayList<Monster> monsters;
	private ArrayList<Treasure> treasures;
	private Scanner in;

	public Application() {
		monsters = new ArrayList<Monster>();
		treasures = new ArrayList<Treasure>();
		in = new Scanner(System.in);
	}

	public void runApp() throws IOException {
		
		Treasure treasure = null;
		Monster monster = null;
		String choice;
		
		System.out.println("*** HELLO - WELCOME TO MONSTER LAND ");
		do {
			printMenu();
			choice = in.nextLine().toUpperCase();

			switch (choice) {

			case "1":
				System.out.println("enter info about the monster");
				monster = new Monster();
				monster.readKeyboard();
				monsters.add(monster);
				break;
			case "2":
				System.out.println("Enter face colour: ");
				String faceColour = in.next();
				monster.setFace(faceColour);
				break;
			case "3":
				System.out.println("Enter hair colour: ");
				String hairColour = in.next();
				monster.setHair(hairColour);
				break;
			case "4":
				System.out.println("enter info about the new treasure");
				treasure = new Treasure();
				treasure.readKeyboard();
				treasures.add(treasure);
				break;
			case "5":
				monster = giveTreasureToMonster(treasure);
				break;
			case "6":
				System.out.println(this.toString());
				break;
			case "7":
				save();
				System.out.println("Monster details saved");
				break;
			case "8":
				load();
				System.out.println("Monster details loaded");
				break;
			case "Q":
				System.out.println("*** THANK YOU FOR USING MONSTER LAND");
				break;
			default:
				System.out.println("not a valid choice");
			}
		} while (!choice.equals("Q"));
	}

	private void load() throws FileNotFoundException {
		
		treasures.clear();
		Scanner scan = new Scanner(new FileReader(TEASURES_FILENAME));
		//int num = scan.nextInt();
		String str = scan.nextLine();
		for (int i = 0; i < 0; i++) {
			scan.nextLine();
			String n = scan.nextLine();
			int v = scan.nextInt();
			Treasure t = new Treasure(n, v);
			treasures.add(t);
		}
		scan.close();

		// Now Monsters and their treasures
		// Clear out the monsters so that we can load afresh again
		monsters.clear();
		scan = new Scanner(new FileReader(MONSTERS_FILENAME));
		scan.useDelimiter("\r?\n|\r"); // UNIX and Windows line endings
		//num = scan.nextInt();
		for (int i = 0; i < 0; i++) {	
			String t = scan.next();
			String h = scan.next();
			String f = scan.next();
			int powerP = scan.nextInt();
			Monster m = new Monster(t, h, f, powerP);

			int numTreasures = scan.nextInt();
			for (int nt = 0; nt < numTreasures; nt++) {
				String toFind = scan.next();
				// Find the treasure in the existing treasures list
				Treasure foundTreasure = findTreasure(toFind);
				m.addTreasure(foundTreasure);
			}
			monsters.add(m);
		}
		scan.close();
	}

	private void save() throws IOException {
		// Treasures first
		int num = treasures.size();
		PrintWriter pw = new PrintWriter(new FileWriter(TEASURES_FILENAME));
		pw.println(num);//
		for (Treasure t : treasures) {
			pw.println(t.getName());
			pw.println(t.getValue());
		}
		pw.close();

		// Now Monsters and their treasures
		num = monsters.size();
		pw = new PrintWriter(new FileWriter(MONSTERS_FILENAME));
		pw.println(num);
		for (Monster m : monsters) {
			pw.println(m.getType());
			pw.println(m.getHair());
			pw.println(m.getFace());
			pw.println(m.getPowerPoints());
			Treasure[] loot = m.getLoot();
			pw.println(loot.length);
			for (Treasure mt : loot) {
				// We just save the name
				pw.println(mt.getName());
			}
		}
		pw.close();

	}

	private Monster giveTreasureToMonster(Treasure treasure) {
		Monster monster = null;
		if (treasure != null) {
			// Find the relevant monster
			System.out.println("Which monster (name)? ");
			String toFind = in.nextLine();
			monster = findMonster(toFind);
			if (monster != null) {
				monster.addTreasure(treasure);
				System.out.println("DONE!");
			} else {
				System.err.println("Couldn't find monster: " + toFind);
			}
		} else {
			System.err.println("You haven't created any new treasure!");
		}
		return monster;
	}

	private Monster findMonster(String toFind) {
		Monster foundMonster = null;
		for (Monster m : monsters) {
			if (m.getType().equals(toFind)) {
				foundMonster = m;
				break;
			}
		}
		return foundMonster;
	}

	private Treasure findTreasure(String toFind) {
		Treasure foundTreasure = null;
		for (Treasure t : treasures) {
			if (t.getName().equals(toFind)) {
				foundTreasure = t;
				break;
			}
		}
		return foundTreasure;
	}

	public void printMenu() {
		System.out
				.println("lots of menu lines \n 1 - create monster \n 2 - change face colour \n"
						+ " 3 - change hair colour \n 4 - create treasure \n 5 - give treasure to a monster \n"
						+ " 6 - print \n 7 - save \n 8 - load \n q = quit");
	}

	public String toString() {
		return "All treasures: " + treasures
				+ " All monsters and their treasures: " + monsters;
	}

	// ////////////////
	public static void main(String args[]) throws IOException {
		Application app = new Application();
		app.runApp();
	}
}
